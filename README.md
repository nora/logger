# nora/logger

## このライブラリの目的

- Monologのセットアップを完結にする
- BearSundayへの組み込みをする

## 使い方想定

ログファイルの吐き出し先だけを指定すると

以下を有効にしたロガーが返却される

- ログローテート機能

```
$logger = (new MakeLogger(path: "path/to/log"))()
```

## BearSunday用のセットアップ

MakeLoggerInterfaceをカスタムクラスへバインドすることで取り替え可能とする。



```
$this->install(new LoggerModule($this->appMeta));
```

