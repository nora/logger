<?php

declare(strict_types=1);

namespace NORA\Logger;

use BEAR\AppMeta\Meta;
use NORA\Logger\Fake\LoggerClient;
use NORA\Logger\Fake\Module\FakeLoggerModule;
use PHPUnit\Framework\TestCase;
use Ray\Di\Injector;

class ModuleTest extends TestCase
{
    private Meta $meta;
    private FakeLoggerModule $module;

    protected function setUp(): void
    {
        $this->meta = new Meta('NORA\\Logger\\Fake', 'app');
        $this->module = new FakeLoggerModule($this->meta);
    }

    public function testInjector(): void
    {
        $injector = new Injector($this->module);
        $loggerClient = $injector->getInstance(LoggerClient::class);
        $loggerClient->info('テストだよ');
        $loggerClient->debug('DEBUGだよ');
    }
}
