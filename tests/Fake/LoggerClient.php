<?php

declare(strict_types=1);

namespace NORA\Logger\Fake;

use Psr\Log\LoggerInterface;
use Ray\Di\Di\Named;

final class LoggerClient
{
    public function __construct(
        #[Named('nora.logger.app')] private LoggerInterface $logger
    ) {
    }

    public function run(): void
    {
        $this->logger->info('info');
    }

    public function info(string $message): void
    {
        $this->logger->info($message);
    }

    public function debug(string $message): void
    {
        $this->logger->debug($message);
    }
}
