<?php

declare(strict_types=1);

namespace NORA\Logger\Fake\Module;

use BEAR\AppMeta\AbstractAppMeta;
use NORA\Logger\Fake\LoggerClient;
use NORA\Logger\LoggerModule;
use Ray\Di\AbstractModule;

final class FakeLoggerModule extends AbstractModule
{
    public function __construct(private AbstractAppMeta $appMeta)
    {
    }

    protected function configure()
    {
        $this->bind(AbstractAppMeta::class)->toInstance($this->appMeta);
        $this->install(new LoggerModule($this->appMeta));
        $this->bind(LoggerClient::class);
    }
}
