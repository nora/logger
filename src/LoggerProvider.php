<?php

declare(strict_types=1);

namespace NORA\Logger;

use Psr\Log\LoggerInterface;
use Ray\Di\ProviderInterface;

/**
 * @template-implements ProviderInterface<LoggerInterface>
 */
final class LoggerProvider implements ProviderInterface
{
    public function __construct(
        private MakeLoggerInterface $makeLogger
    ) { }

    public function get(): LoggerInterface
    {
        return ($this->makeLogger)();
    }
}
