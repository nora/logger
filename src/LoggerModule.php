<?php

declare(strict_types=1);

namespace NORA\Logger;

use BEAR\AppMeta\AbstractAppMeta;
use Monolog\Level;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Ray\Di\AbstractModule;
use Ray\Di\Scope;

final class LoggerModule extends AbstractModule
{

    public function __construct(private AbstractAppMeta $appMeta, self $module = null)
    {
        parent::__construct($module);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->bind(Logger::class)->annotatedWith('nora.logger.base')->toConstructor(Logger::class, [
            'name' => 'nora.logger.name'
        ]);
        $this->bind(AbstractAppMeta::class)->toInstance($this->appMeta);
        $this->bind(MakeLoggerInterface::class)->toConstructor(MakeLogger::class, [
            "name" => "nora.logger.name",
            "maxFiles" => "nora.logger.maxFiles",
            "filename" => "nora.logger.filename",
            "level" => "nora.logger.level",
            "path" => "nora.logger.path",
        ]);
        $this->bind()->annotatedWith('nora.logger.name')->toInstance('nora');
        $this->bind()->annotatedWith('nora.logger.maxfiles')->toInstance(30);
        $this->bind()->annotatedWith('nora.logger.level')->toInstance(Level::Debug);
        $this->bind()->annotatedWith('nora.logger.filename')->toInstance("app.log");
        $this->bind()->annotatedWith('nora.logger.path')->toInstance($this->appMeta->logDir);

        $this
            ->bind(LoggerInterface::class)
            ->annotatedWith('nora.logger.app')
            ->toProvider(LoggerProvider::class)
            ->in(Scope::SINGLETON);
    }
}
