<?php

namespace NORA\Logger;

use Psr\Log\LoggerInterface;

trait LoggerClientTrait
{
    private bool $logger_disabled = false;
    private ?LoggerInterface $logger = null;

    private function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    private function logDebug(string $message)
    {
        if ($this->logger) {
            assert($this->logger instanceof LoggerInterface);
            $this->logger->debug($message);
        }
    }

    private function logNotice(string $message)
    {
        if ($this->logger) {
            assert($this->logger instanceof LoggerInterface);
            $this->logger->notice($message);
        }
    }

    private function logWarning(string $message)
    {
        if ($this->logger) {
            assert($this->logger instanceof LoggerInterface);
            $this->logger->warning($message);
        }
    }

    private function logError(string $message)
    {
        if ($this->logger) {
            assert($this->logger instanceof LoggerInterface);
            $this->logger->error($message);
        }
    }
}
