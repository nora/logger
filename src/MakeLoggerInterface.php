<?php

declare(strict_types=1);

namespace NORA\Logger;

use Monolog\Logger;

interface MakeLoggerInterface
{
    public function __invoke(): Logger;
}
