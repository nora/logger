<?php

namespace NORA\Logger;

use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Level;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;

class MakeLogger implements MakeLoggerInterface
{
    public function __construct(
        private string $path = "./",
        private int $maxFiles = 30,
        private Level $level = Level::Debug,
        private string $filename = "app.log",
        private string $name = "app",
    ) {
    }

    public function __invoke(): Logger
    {
        $introspectionProcessor = new IntrospectionProcessor(
            Level::Debug,
            [Logger::class]
        );
        $path = $this->path . '/' . $this->filename;
        $routing = new RotatingFileHandler($path, $this->maxFiles, $this->level);
        $routing->setFilenameFormat('{filename}_{date}', 'Ymd');
        $routing->setFormatter(new LineFormatter("%datetime% %channel% [%level_name%] %extra.class%::%extra.function%(%extra.line%) %message%  %context%\n"));

        $logger = new Logger($this->name, [$routing], [$introspectionProcessor]);
        return $logger;
    }
}
